<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kegiatan;
use App\User;


class KegiatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kegiatans = Kegiatan::all();
        return view('user.kegiatan.show', compact('kegiatans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.kegiatan.kegiatan');
               
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::all();
        $kegiatan = new Kegiatan;
            $kegiatan->kegiatan         = $request->kegiatan;
            $kegiatan->hasil            = $request->hasil;
            $kegiatan->tindaklanjut     = $request->tindaklanjut;
            $kegiatan->tanggal          = $request->tanggal;
            $kegiatan->user_id          = $request->user_id;
        $kegiatan->save();
        return redirect(route('kegiatan.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kegiatan = Kegiatan::where('id',$id)->first();
        return view('user.kegiatan.edit',compact('kegiatan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kegiatan =  Kegiatan::find($id);
            $kegiatan->kegiatan         = $request->kegiatan;
            $kegiatan->hasil            = $request->hasil;
            $kegiatan->tindaklanjut     = $request->tindaklanjut;
            $kegiatan->tanggal          = $request->tanggal;
            $kegiatan->user_id          = $request->user_id;
        $kegiatan->update();
        return redirect(route('kegiatan.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        kegiatan::where('id',$id)->delete();
        return redirect()->back();
    }
}
