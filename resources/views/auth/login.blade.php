<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="/css/materialize/css/materialize_OLD.css">
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <style type="text/css">
    body {
    background-image: url('/img/bg_login.jpg');
    background-size: cover;
    }
    </style>
    <title>Login page</title>
</head>
<body>
<div id="jqueryvalidation" class="section">
            <div class="row">
              <div class="col s12">
                  <div class="col s12 m4 offset-m8">
                        <div class="card-panel">
                            <h4 class="center" style="margin-top: 148px;">Login Dulu!!!</h4>

                      <div class="row">
                            <form method="POST" action="{{ route('login') }}">
                            @csrf
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>   
                                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                            <div class="errorTxt1"></div>
                                        </div>
                                        <div class="input-field col s12">
                                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                                            
                                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                            <div class="errorTxt2"></div>
                                        </div>
                                        <div class="row">
                                        <div class="input-field col s8 m4">
                                            <button class="btn waves-effect waves-light submit" type="submit" name="action">Submit
                                            </button>
                                        </div>
                                        <div class="col s4 m8"><p style="font-size: 12.5px;">Don't have account? <a href="/register">SignUp Now!</a></p></div></div>
                                    </div>
                           
                            </div>
                        </div>
                    </div>
                </div>
            </div>

</body>
</html>
    <!-- jQuery Library -->
    <script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>    
    <!--angularjs-->
    <script type="text/javascript" src="js/plugins/angular.min.js"></script>
    <!--materialize js-->
    <script type="text/javascript" src="js/materialize.js"></script>
    <!--prism -->
    <script type="text/javascript" src="js/plugins/prism/prism.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!-- chartist -->
    <script type="text/javascript" src="js/plugins/chartist-js/chartist.min.js"></script>
    
    <!-- chartist -->
    <script type="text/javascript" src="js/plugins/jquery-validation/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/plugins/jquery-validation/additional-methods.min.js"></script>
    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="js/plugins.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="js/custom-script.js"></script>
    <script type="text/javascript">
