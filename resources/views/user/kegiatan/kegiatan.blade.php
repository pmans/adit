@extends('user.layouts.app')

@section('main-content')  
<div id="jqueryvalidation" class="section">
  <div class="row">
    <div class="container">
      <div class="col s12 m12 l12">
        <div class="col s12">
          <div class="card-panel" style="margin-top: 37px;">
            <h4 class="header2">Tambah Kegiatan</h4>
            <div class="row">
              <form role="form" action="{{ route('kegiatan.store') }}" method="POST">
                {{ csrf_field() }}
                <div class="row">
                  <div class="input-field col s12">
                    <label for="kegiatan">Kegiatan:</label>
                    <input id="kegiatan" name="kegiatan" type="text" data-error=".errorTxt1">
                    <div class="errorTxt1"></div>
                  </div>
                  <div class="input-field col s12">
                    <label for="hasil">Hasil:</label>
                    <input id="hasil" type="text" name="hasil" data-error=".errorTxt2">
                    <div class="errorTxt2"></div>
                  </div>
                  <div class="input-field col s12">
                    <label for="tindaklanjut">Tindak Lanjut:</label>
                    <input id="tindaklanjut" type="text" name="tindaklanjut" data-error=".errorTxt3">
                    <div class="errorTxt3"></div>
                  </div>
                  <div class="input-field col s12">
                    <label for="tanggal">Tanggal:</label>
                    <input id="tanggal" class="datepicker" type="date" name="tanggal" data-error=".errorTxt4">
                    <div class="errorTxt4"></div>
                  </div>

                  <input type="hidden"  name="user_id" value="{{ Auth::user()->id }} ">
                  
                </div>
                <div class="input-field col s12">
                  <button class="btn waves-effect waves-light right submit" type="submit" name="action">Submit
                    <i class="mdi-content-send right"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>  
    </div>
  </div>
</div>
</div>
</div>
</div>


@endsection
