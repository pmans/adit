@extends('user.layouts.app')

@section('main-content')  
<div id="table-datatables">
  <h4 class="header center">Data Kegiatan</h4>
  <div class="row">
    <div class="col s12 m12">
      <table id="data-table-simple" class="responsive-table display" cellspacing="0">
        <thead>
          <tr>
            <th>ID</th>
            <th>Kegiatan</th>
            <th>Hasil</th>
            <th>Tindak Lanjut</th>
            <th>Tanggal</th>
            <th>Action</th>

          </tr>
        </thead>
        
        <tbody>

          @foreach($kegiatans as $kegiatan)
          @if ( Auth::user()->id == $kegiatan->user_id)
          <tr>
            <td>{{ $loop->index +1 }}</td>
            <td>{{ $kegiatan->kegiatan }}</td>
            <td>{{ $kegiatan->hasil }}</td>
            <td>{{ $kegiatan->tindaklanjut }}</td>
            <td>{{ $kegiatan->tanggal }}</td>
            <td><a href="{{ route('kegiatan.edit', $kegiatan->id) }}">Edit</a> | 
              <form method="POST" id="delete-form-{{ $kegiatan->id }}" action="{{ route('kegiatan.destroy', $kegiatan->id) }}" style="display: none">
                {{ csrf_field() }} {{ method_field('DELETE') }}
              </form>
              <a  type="button" onclick="
              if(confirm('Are you sure, want to delete {{ $kegiatan->kegiatan }}')) {
                event.preventDefault(); document.getElementById('delete-form-{{ $kegiatan->id }}').submit();
              } else {
                event.preventDefault();
              }
              ">Delete</a></td>

            </tr>
            @endif
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div> 



  @endsection
