    <!-- jQuery Library -->
    <script type="text/javascript" src="{{asset('js/plugins/jquery-1.11.2.min.js')}}"></script>    
    <!--angularjs-->
    <script type="text/javascript" src="{{asset('js/plugins/angular.min.js')}}"></script>
    <!--materialize js-->
    <script type="text/javascript" src="{{asset('js/materialize.js')}}"></script>
    <!--prism -->
    <script type="text/javascript" src="{{asset('js/plugins/prism/prism.js')}}"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="{{asset('js/plugins/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
    <!-- chartist -->
    <script type="text/javascript" src="{{asset('js/plugins/chartist-js/chartist.min.js')}}"></script>
    
    <!-- chartist -->
    <script type="text/javascript" src="{{asset('js/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/plugins/jquery-validation/additional-methods.min.js')}}"></script>
    
    <script type="text/javascript" src="{{asset('js/plugins/data-tables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/plugins/data-tables/data-tables-script.js')}}"></script>
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="{{asset('js/plugins.js')}}"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="{{asset('js/custom-script.js')}}"></script>
    <script type='text/javascript'>
    $(window).load(function() {
        setTimeout(function() {
            Materialize.toast('<div id="card-alert" class="card green"><div class="card-content white-text"><p> SUCCESS to LOGIN</p></div>', 1500);
        }, 1500);
      });
    </script>