<link rel="stylesheet" type="text/css" href="{{asset('css/materialize.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
<!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
<link href="{{asset('js/plugins/perfect-scrollbar/perfect-scrollbar.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
<link href="{{asset('js/plugins/jvectormap/jquery-jvectormap.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
<link href="{{asset('js/plugins/chartist-js/chartist.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
<link href="{{asset('js/plugins/data-tables/css/jquery.dataTables.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">