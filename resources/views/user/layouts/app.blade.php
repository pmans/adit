<!DOCTYPE html>
<html lang="en">
<head>
    @include('user.layouts.head')
</head>
<body>
    <div class="wrapper">
        @include('user.layouts.header')
        @include('user.layouts.sidebar')

        @section('main-content')
        @show
        

        @include('user.layouts.footer')
        
    </div>
</body>
</html>